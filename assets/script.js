
// Esta es una funcion declarada que recibe como parametro un objeto db y un objeto contacto
// la funcion guarda en el local storage el objeto contacto (clave-valor)
const guardarContacto = (db, contacto) => {
    db.setItem(contacto.id, JSON.stringify(contacto))
    window.location.href = 'index.html'
};

// Esta funcion obtiene los elementos (nombre, telefono, email) y verifica que no esten vacios.
// si no estan vacios, crea un objeto contacto con los valores de los elementos y llama a la funcion guardarContacto
// si estan vacios, muestra un mensaje de alerta con sweetalert
function agregarContacto() {
    const nombre = document.getElementById('nombre');
    const telefono = document.getElementById('telefono');
    const email = document.getElementById('email');

    if (nombre.value.trim() !== "" && telefono.value.trim() !== "" && email.value.trim() !== ""){

        const nombreContacto = nombre.value;
        const telefonoContacto = telefono.value;
        const emailContacto = email.value;

        const contacto = {
            id: Date.now(),
            nombre: nombreContacto,
            telefono: telefonoContacto,
            email: emailContacto,
        };
        guardarContacto(localStorage, contacto);
    } else {
        Swal.fire({
            title: "Ups! te faltó algún campo",
            text: "Debes ingresar todos los campos",
            icon: "warning"
          });    
    
    };
};

// Esta funcion recorre los valores del local storage y los convierte en un objeto
// luego crea una fila de tabla con los valores del objeto y los agrega al html(innerHtml), usando las backticks para concatenar.
// por ultimo, agrega un boton de borrar que llama a la funcion eliminarContacto, pasandole el id del contacto, ademas agregue un icono para borrar
function mostrarContactos() {
    const contactos = Object.values(localStorage)
    const tablaContactos = document.getElementById('contactos')

    for (const contacto of contactos) {
        const contactoObj = JSON.parse(contacto)
        const tr = document.createElement('tr')
        tr.innerHTML = `
            <td class="text-center">${contactoObj.nombre}</td>
            <td class="text-center">${contactoObj.telefono}</td>
            <td class="text-center">${contactoObj.email}</td>
        `
        tr.innerHTML += `
            <td class="text-center">
                <a class="btn btn-danger btn-sm" data-id="${contactoObj.id}" onclick="eliminarContacto.call(this)"><img src="./assets/img/borrar.png" alt="borrar"></a>
            </td>
        `
        tablaContactos.appendChild(tr)
    };
};

// Esta funcion recibe el id del contacto y lo elimina del local storage, usando el atrituto data-id visto en clase.
function eliminarContacto() {
    const id = this.getAttribute('data-id')
    localStorage.removeItem(id)
    window.location.href = 'index.html'
};

// Esta funcion recibe el id del contacto y lo busca en el local storage, luego lo convierte en un objeto y lo retorna.
function buscarContacto(){
    const input = document.getElementById('buscar');
    const filtro = input.value.toUpperCase();
    const tabla = document.getElementById('contactos');
    const tr = tabla.getElementsByTagName('tr');

    for (let i = 0; i < tr.length; i++) {
        let td = tr[i].getElementsByTagName('td')[0];
        if (td) {
            let textValue = td.textContent || td.innerText;
            if (textValue.toUpperCase().indexOf(filtro) > -1) {
                tr[i].style.display = '';
            } else {
                tr[i].style.display = 'none';
            };
        };
    };
};

// Esta funcion limpia los campos de nombre, telefono y email
// setea los valores en vacio.
function limpiarCampos() {
    document.getElementById('nombre').value = '';
    document.getElementById('telefono').value = '';
    document.getElementById('email').value = '';
};

// funcion para contar los contactos en el local storage y mostrar el total en el html.
function totalContactos() {
    const contactos = Object.values(localStorage)
    const total = document.getElementById('totalContactos')
    total.innerHTML = contactos.length
}

// Esta funcion limpia todo el local storage y redirige a la pagina principal.
// ademas muestra un mensaje de alerta con sweetalert para confirmar la eliminacion.
// pero antes verifica si hay contactos en la agenda, si no hay, muestra un mensaje de alerta.
function alvTodo() {

    if (localStorage.length === 0) {
        Swal.fire({
            title: "No hay nada que borrar",
            text: "No tienes contactos",
            icon: "warning"
          });
          return
    } else {

        Swal.fire({
            title: "¿Estás seguro?",
            text: "Después no habrá vuelta atrás!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, borrar!"
        }).then((result) => {
            if (result.isConfirmed) {
            Swal.fire({
                title: "Borrando la matrix...",
                text: "Sigue al conejo blanco...",
                icon: "success"
            });
                localStorage.clear()
                setTimeout(() => {
                window.location.href = 'index.html'
                }, 2000);
            };
        });
    };
};

// funcion easteregg, muestra un mensaje de alerta con sweetalert
function wakeUpNeo() {
    
    Swal.fire({
        title: "Despierta Neo!",
        text: "La Matrix te tiene...",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3C52C3",
        cancelButtonColor: "#d92e20",
        confirmButtonText: "Despertar",
        cancelButtonText: "Dormir"
      }).then((result) => {
        if (result.isConfirmed) {
            playAudio();
            Swal.fire({
                title: "Bienvenido a la realidad",
                text: "La Matrix existe!",
                icon: "success",
                
            });
        } else {
            Swal.fire({
                title: "Duerme Neo",
                text: "La Matrix te tiene...",
                icon: "error"
            });
        };
      });    
};

// funcion para reproducir audio
function playAudio() {
    const playAudio = document.getElementById('audio');
    playAudio.volume = 0.3;
    playAudio.play();
}

// Esta funcion se ejecuta cuando el documento esta cargado
// llama a la funcion mostrarContactos para mostrar los contactos en la tabla.
document.addEventListener('DOMContentLoaded', (event) => {
    mostrarContactos();
    totalContactos();
});